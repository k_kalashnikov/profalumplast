﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mst.Core.ViewModels;
using Mst.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace Mst.Web.Models
{
    public class AccessCreateOrEditModel : BaseModelViewModel<ApplicationAccess>
    {
        public AccessCreateOrEditModel() : base()
        {
            Rules = new List<string>();
            PosibleRules = new List<ApplicationRole>();
        }

        public AccessCreateOrEditModel(RoleManager<ApplicationRole> _roleManager, ApplicationAccess _model) : base(_model)
        {
            Rules = model.Roles?.Select(m => m.RoleId)?.ToList() ?? new List<string>();
            PosibleRules = _roleManager.Roles.ToList() ?? new List<ApplicationRole>();
        }

        public List<ApplicationRole> PosibleRules { get; set; }
        public List<string> Rules { get; set; }
    }
}
