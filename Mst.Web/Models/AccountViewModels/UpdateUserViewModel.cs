﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Mst.Core.Models;
using Mst.Core.ViewModels;

namespace Mst.Web.Models.AccountViewModels
{
    public class UpdateUserViewModel:BaseModelViewModel<ApplicationUser>
    {
        public UpdateUserViewModel() : base()
        {
            PosibleAccesses = new List<ApplicationAccess>();
        }

        public UpdateUserViewModel(ApplicationUser _model) : base(_model)
        {
            PosibleAccesses = new List<ApplicationAccess>();
            Accesses = _model.Accesses?.Select(m => m.AccessId).ToList() ?? new List<int>();
        }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        public List<ApplicationAccess> PosibleAccesses { get; set; }
        public List<int> Accesses { get; set; }
    }
}
