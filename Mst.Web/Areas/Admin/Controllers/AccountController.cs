﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Mst.Core.Controllers;
using Mst.Core.Models;
using Mst.Web.Models.AccountViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Mst.Web.Areas.Admin.Controllers
{
    public class AccountController: BaseController
    {
        public SignInManager<ApplicationUser> signInManager { set; get; }
        public UserManager<ApplicationUser> userManager { set; get; }
        public AccountController(ApplicationDbContext _context, SignInManager<ApplicationUser> _signInManager, UserManager<ApplicationUser> _userManager) : base(_context)
        {
            signInManager = _signInManager;
            userManager = _userManager;
        }
        
        [HttpGet]
        [AllowAnonymous]
        public IActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult LogIn(LoginViewModel _formResult)
        {
            if (ModelState.IsValid)
            {
                var result = signInManager.PasswordSignInAsync(_formResult.Email, _formResult.Password, _formResult.RememberMe, false).Result;
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                return BadRequest("Invalid email or password");
            }
            return BadRequest(GetModelStateErrors(ModelState));

        }

        public async Task<IActionResult> LogOut()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("LogIn");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Registration(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email
                };
                var result = userManager.CreateAsync(user, model.Password).Result;
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                return BadRequest(ModelState);
            }
            return BadRequest(ModelState);
        }

        
    }
}
