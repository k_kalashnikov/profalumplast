﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mst.Core.Controllers;
using Mst.Core.Models;
using Mst.Core.Tools.DataGeneric;


namespace Mst.Web.Areas.Admin.Controllers
{
    public class DataController : BaseController
    {
        public DataGeneric<ApplicationDbContext> generator { get; set; }
        public DataController(ApplicationDbContext _context) : base(_context)
        {
            generator = new DataGeneric<ApplicationDbContext>(context);
        }

        public IActionResult Index()
        {
            var result = generator.GetDataList();
            return View(result);
        }
    }
}
