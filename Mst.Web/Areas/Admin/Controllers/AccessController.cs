﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mst.Core.Controllers;
using Mst.Core.Models;
using Mst.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Mst.Web.Areas.Admin.Controllers
{
    public class AccessController : BaseAdminModelController<ApplicationAccess,int>
    {
        public RoleManager<ApplicationRole> roleManager { get; set; }
        public UserManager<ApplicationUser> userManager { get; set; }
        public AccessController(ApplicationDbContext _context, RoleManager<ApplicationRole> _roleManager, UserManager<ApplicationUser> _userManager) : base(_context)
        {
            roleManager = _roleManager;
            userManager = _userManager;
        }

        [HttpGet]
        public override IActionResult CreateOrEdit(int id)
        {
            var model = entity.GetById(id) ?? new ApplicationAccess();
            var result = new AccessCreateOrEditModel(roleManager, model);
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AccessCreateOrEdit(AccessCreateOrEditModel _formResult)
        {
            if (ModelState.IsValid)
            {
                var model = _formResult.model;
                var roleAccesses = context.Set<RoleAccess>();
                var userAccesses = context.Set<UserAccess>();
                var roleList = roleAccesses.Find(m => (m.AccessId == model.Id) && (!_formResult.Rules.Contains(m.RoleId)))?.Select(m => m.Role).Distinct()?.ToList() ?? new List<ApplicationRole>();
                var userList = userAccesses.Find(m => m.AccessId == model.Id)?.Select(m => m.User)?.Distinct()?.ToList() ?? new List<ApplicationUser>();
                foreach (var itemUser in userList)
                {
                    foreach (var itemRole in roleList)
                    {
                        if (IsOnly(itemUser, itemRole, model.Id))
                        {
                            var result = userManager.RemoveFromRoleAsync(itemUser, itemRole.Name).Result;
                        }
                    }
                }
                (roleAccesses.Find(m => m.AccessId == model.Id)?.ToList() ?? new List<RoleAccess>()).ForEach(m => context.Remove(m));
                context.SaveChanges();
                model.Roles = new List<RoleAccess>();
                foreach (var itemRoleId in _formResult.Rules)
                {
                    var currentRole = roleManager.FindByIdAsync(itemRoleId).Result;
                    var temp = new RoleAccess()
                    {
                        Access = model,
                        Role = currentRole
                    };
                    model.Roles.Add(temp);
                    context.Add(temp);
                    foreach (var itemUser in userList)
                    {
                        if (!userManager.IsInRoleAsync(itemUser, currentRole.Name).Result)
                        {
                            var result = userManager.AddToRoleAsync(itemUser, currentRole.Name).Result;
                        }
                    }
                }
                var resultUpdate = (entity.IsExist(model)) ? entity.Refresh(model) : entity.Append<ApplicationAccess>(model);
                context.SaveChanges();
                return RedirectToAction("CreateOrEdit", new { id = resultUpdate.Model.Id });
            }
            return BadRequest(ModelState);
        }

        public bool IsOnly(ApplicationUser _user, ApplicationRole _role, int _accessId)
        {
            if (!userManager.IsInRoleAsync(_user, _role.Name).Result)
            {
                return false;
            }

            var roleAccesses = context.Set<RoleAccess>().Find(m => m.RoleId.Equals(_role.Id)).ToList();
            var userAccesses = context.Set<UserAccess>().Find(m => m.UserId.Equals(_user.Id)).ToList();
            var accessList = entity.Find(m => m.Roles.Intersect(roleAccesses).Any() && m.Users.Intersect(userAccesses).Any());
            if (accessList.Count() > 1)
            {
                return false;
            }
            return true;
        }
    }
}
