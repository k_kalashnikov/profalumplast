using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mst.Core.Models;
using Mst.Core.Controllers;
using Mst.Core.ViewModels;
using Mst.Web.Models.AccountViewModels;

namespace Mst.Web.Areas.Admin.Controllers
{
    public class UserController : BaseAdminModelController<ApplicationUser, string>
    {
        public UserManager<ApplicationUser> userManager { get; set; }
        public RoleManager<ApplicationRole> roleManager { get; set; }

        public UserController(ApplicationDbContext _context, UserManager<ApplicationUser> _userManager, RoleManager<ApplicationRole> _roleManager) : base(_context)
        {
            userManager = _userManager;
            roleManager = _roleManager;
        }

        [HttpGet]
        public override IActionResult CreateOrEdit(string id) 
        {
            return (!entity.IsExist(id))
                ? View("Create", new CreateUserViewModel())
                : View("Edit", new UpdateUserViewModel(entity.GetById(id))
                {
                    PosibleAccesses = context.Accesses.GetAll().ToList()
                });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UserCreate(CreateUserViewModel _formResult)
        {            
            if (ModelState.IsValid)
            {
                _formResult.model.UserName = _formResult.model.Email;
                var result = userManager.CreateAsync(_formResult.model, _formResult.Password).Result;
                if (result.Succeeded)
                    return RedirectToAction("Index", "User");
                return BadRequest(GetIndentityResultErrors(result));
            }
            return BadRequest(GetModelStateErrors(ModelState));
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UserEdit(UpdateUserViewModel _formResult)
        {
            if (ModelState.IsValid)
            {
                context.UserAccess.Find(m => m.UserId.Equals(_formResult.model.Id)).ToList().ForEach(m => context.Remove(m));
                context.SaveChanges();
                _formResult.model.Accesses = new List<UserAccess>();
                foreach (var item in _formResult.Accesses)
                {
                    var curAccess = context.Accesses.GetById(item);
                    var temp = new UserAccess()
                    {
                        User = _formResult.model,
                        Access = curAccess
                    };
                    context.Add(temp);
                    _formResult.model.Accesses.Add(temp);
                    foreach (var itemRole in curAccess.Roles)
                    {
                        if (!userManager.IsInRoleAsync(_formResult.model, itemRole.Role.Name).Result)
                        {
                            userManager.AddToRoleAsync(_formResult.model, itemRole.Role.Name).Wait();
                        }
                    }
                }
                entity.Refresh(_formResult.model);
                context.SaveChanges();
                if (!string.IsNullOrEmpty(_formResult.Password))
                {
                    userManager.ChangePasswordAsync(_formResult.model, _formResult.OldPassword, _formResult.Password).Wait();
                }
                return RedirectToAction("CreateOrEdit", new { id = _formResult.model.Id });
            }
            return BadRequest(ModelState);
        }

    }
}