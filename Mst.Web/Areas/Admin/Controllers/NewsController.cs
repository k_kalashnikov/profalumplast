﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mst.Core.Controllers;
using Mst.Core.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Mst.Web.Areas.Admin.Controllers
{
    public class NewsController : BaseAdminModelController<News, int>
    {
        public NewsController(ApplicationDbContext _context) : base(_context)
        {

        }
    }
}
