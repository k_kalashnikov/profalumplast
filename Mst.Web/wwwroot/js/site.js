﻿window.delayedFunction = new Object;
var timeOutId;

function TableObject(element) {
    var self = this;
    self.Obj = $(element);

    self.Init = function () {

        self.table = self.Obj.DataTable({
            stateSave: true,
            responsive: true,
            language: {
                "processing": "Подождите...",
                "search": "Поиск:",
                "lengthMenu": "Показать _MENU_ записей",
                "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
                "infoEmpty": "Записи с 0 до 0 из 0 записей",
                "infoFiltered": "(отфильтровано из _MAX_ записей)",
                "infoPostFix": "",
                "loadingRecords": "Загрузка записей...",
                "zeroRecords": "Записи отсутствуют.",
                "emptyTable:": "В таблице отсутствуют данные",
                "paginate": {
                    "first": "Первая",
                    "previous": "Предыдущая",
                    "next": "Следующая",
                    "last": "Последняя"
                },
                "aria": {
                    "sortAscending": ": активировать для сортировки столбца по возрастанию",
                    "sortDescending": ": активировать для сортировки столбца по убыванию"
                }
            }
        });

        self.Obj.find('.delete').click(function () {
            var deleteLink = $(this);
            if (!confirm("Вы действительно хотите удалить элемент?")) {
                return false;
            }
            var dataPost = deleteLink.attr('data-post');
            var url = deleteLink.attr('href');
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    id: dataPost
                },
                success: function (html) {
                    deleteLink.closest('tr').css('background-color', '#ff5555');
                    deleteLink.parent().html('<span>УДАЛЁН</span>');
                },
                error: function (message) {
                    console.log(message);
                }
            });
            return false;
        });;
    }

    self.Init();
}

function FileSearcher(element) {
    var self = this;
    self.Obj = $(element);
    self.Header = self.Obj.find('.file-search-header');
    self.Body = self.Obj.find('.file-search-body');
    self.Footer = self.Obj.find('.file-search-footer');
    self.ButtonRefresh = self.Obj.find('#button-refresh');
    self.ButtonChouse = self.Obj.find('#button-chouse');
    self.ButtunCreate = self.Obj.find('#button-create');
    self.NewFolderInput = self.Obj.find('#new-folder-input');
    self.CurrentPath = "";
    self.RootPath = "";
    self.BodyContent = [];
    self.NeededType = "";
    self.Target;

    self.Refresh = function () {
        $.ajax({
            url: '/Directories/GetFolderContent',
            type: 'POST',
            data: {
                _folderPath: self.CurrentPath
            },
            success: function (result) {
                self.CurrentPath = result.currentPath;
                self.SetRootPath();
                self.BodyContent = [];

                var rootFolder = new DirectoryObject();
                rootFolder.Name = '..';
                rootFolder.Type = 'root';
                rootFolder.Path = self.RootPath;
                rootFolder.Obj.dblclick(function () {
                    clearTimeout(timeOutId);
                    clearTimeout(timeOutId - 1);
                    self.CurrentPath = rootFolder.Path;
                    self.Refresh();
                });

                self.BodyContent.push(rootFolder);

                var directories = result.directories;
                var files = result.files;
                self.SetBodyContent(directories, 'directory');
                self.SetBodyContent(files, 'file');
                var newContent = $('<ul>');
                newContent.css('list-style-type', 'none');
                self.BodyContent.forEach(function (item) {
                    newContent.append(item.Render());
                });

                self.Body.html("");
                self.Body.append(newContent);

            },
            error: function (message) {
                alert("Доступ запрещён");
                console.log(message);
            }
        });
    }

    self.Init = function () {
        self.Body.css('height', (self.Obj.width() / 2));
        self.ShutDown();
    }

    self.Debug = function () {
        console.log(self);
    }

    self.SetBodyContent = function (arr, type) {
        arr.forEach(function (item) {
            var temp = new DirectoryObject();
            var tempName = item.split('\\');
            temp.Name = tempName[tempName.length - 1];
            temp.Type = type;
            temp.Path = item;
            if (type == "directory") {
                temp.Obj.dblclick(function () {
                    clearTimeout(timeOutId);
                    clearTimeout(timeOutId - 1);
                    self.CurrentPath = temp.Path;
                    self.Refresh();
                });
            }
            self.BodyContent.push(temp);
        });

    }

    self.SetRootPath = function () {
        var pathArr = self.CurrentPath.split('\\');
        console.log(pathArr);
        self.RootPath = "";
        for (var i = 0; i < pathArr.length - 1; i++) {
            if (i == 0) {
                self.RootPath = self.RootPath + pathArr[i];
                continue;
            }
            self.RootPath = self.RootPath + "\\" + pathArr[i];
        }
        if (pathArr.length == 2) {
            self.RootPath = self.RootPath + "\\";
        }
        console.log(self.RootPath);
    }

    self.ShutDown = function () {
        self.CurrentPath = "";
        self.RootPath = "";
        self.BodyContent = [];
        self.NeededType = "";
        self.Target = undefined;
        self.Obj.hide();
    }

    self.TurnOn = function (target, type) {
        self.Target = $(target);
        self.NeededType = type;
        self.Obj.show();
        self.Refresh();
    }

    self.ButtonRefresh.click(function () {
        self.Refresh();
    });

    self.ButtonChouse.click(function () {
        var chousenFolder;
        self.BodyContent.forEach(function (item) {
            if (item.Obj.hasClass('active') && item.Type == self.NeededType) {
                chousenFolder = item;
            }
        });
        if (chousenFolder == undefined) {
            alert('Выберите элемент типа: ' + self.NeededType);
            return false;
        }

        self.Target.val(chousenFolder.Path);
        self.Target.change();
        self.ShutDown();
    });

    self.ButtunCreate.click(function () {
        var newName = self.NewFolderInput.val() + "";
        if (newName.length < 1) {
            alert("Введите имя папки");
            return false;
        }

        var tempName = self.CurrentPath.split('\\');
        var folderName = "";
        for (var i = 0; i < tempName.length; i++) {
            if (!(tempName[i].length >= 1)) {
                continue;
            }
            if (i == 0) {
                folderName = folderName + tempName[i];
                continue;
            }
            folderName = folderName + '\\' + tempName[i];
        }
        folderName = folderName + '\\' + newName;
        $.ajax({
            url: '/Directories/CreateNewFolder',
            type: 'POST',
            data: {
                _folderName: folderName
            },
            success: function () {
                self.Refresh();
            },
            error: function (message) {
                alert("Произошла ошибка: " + message);
                console.log(message);
            }
        });
    });

    self.Init();
}

function DirectoryObject() {
    var self = this;
    self.Name = "";
    self.Children = [];
    self.Path = "";
    self.Type = "";
    self.Obj = $('<li>');

    self.Render = function () {
        self.Obj.text(" " + self.Name);
        self.Obj.css('cursor', 'poiter');
        self.Obj.attr('data-type', self.Type);
        switch (self.Type) {
        case 'directory':
            self.Obj.prepend('<i class="fa fa-folder"></i>');
            break;
        case 'root':
            self.Obj.prepend('<i class="fa fa-folder-open"></i>');
            break;
        case 'file':
            self.Obj.prepend('<i class="fa fa-file-o"></i>');
            break;
        default:
            break;
        }
        return self.Obj;
    }

    self.Debug = function () {
        console.log("Name: " + self.Name);
        console.log("Children: " + self.Children);
    }

    self.Obj.click(function () {
        timeOutId = setTimeout(self.Change(), 500);
    });

    self.Change = function () {
        var parent = self.Obj.parent();
        parent.children('li').each(function () {
            $(this).removeClass('active');
        });
        self.Obj.addClass('active');
    }
}

function MultiSelectItems(element)
{
    var self = this;
    self.Obj = $(element);
    self.Href = self.Obj.attr('data-href');
    self.Check = self.Obj.find('.fa-check');

    self.Init = function ()
    {
        var selectObj = $('option[data-href="' + self.Href + '"]');
        self.Check.css('float', 'right');
        if (selectObj.attr('selected') == 'selected') {
            self.Obj.addClass('list-group-item-success');
            self.Check.show();
        }
    }

    self.Chouse = function () {
        if (self.Obj.hasClass('list-group-item-success')) {
            self.Obj.removeClass('list-group-item-success');
            $('option[data-href="' + self.Href + '"]').removeAttr('selected');
            $('option[data-href="' + self.Href + '"]').prop('selected', false);
            self.Check.hide();
        }
        else {
            self.Obj.addClass('list-group-item-success');
            $('option[data-href="' + self.Href + '"]').attr('selected', 'selected');
            $('option[data-href="' + self.Href + '"]').prop('selected', true);
            self.Check.show();
        }
    }

    self.Obj.click(function () {
        self.Chouse();
    });

    self.Init();
}

$(window).on("load", function () {
    console.log("load");
    for (var item in window.delayedFunction) {
        window.delayedFunction[item]();
        console.log(item + " comlited.");
    }
});