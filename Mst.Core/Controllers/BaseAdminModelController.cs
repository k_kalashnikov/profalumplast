﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mst.Core.Models;
using Mst.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;
using Mst.Core.Extentions;

namespace Mst.Core.Controllers
{
    [Area("Admin")]
    public class BaseAdminModelController<TEntity, TPK> : BaseModelController<TEntity, TPK> where TEntity : class, IBaseModel<TEntity>
    {
        public BaseAdminModelController(ApplicationDbContext _context) : base(_context)
        {
        }

        [HttpGet]
        public virtual IActionResult Index()
        {
            var models = entity.GetAll().ToList().Cast<object>().ToList();
            var result = new IndexTableViewModel(models);
            return View(result);
        }

        [HttpGet]
        public virtual IActionResult CreateOrEdit(TPK id)
        {

            var model = entity.GetById(id) ?? Activator.CreateInstance<TEntity>();
            var result = new BaseModelViewModel<TEntity>(model);
            return View(string.IsNullOrEmpty(id.ToString()) ? "Create" : "Edit", result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual IActionResult CreateOrEdit(BaseModelViewModel<TEntity> _formResult)
        {
            if (ModelState.IsValid)
            {
                var result = entity.IsExist(_formResult.model) ? entity.Refresh(_formResult.model) : entity.Append(_formResult.model);
                context.SaveChanges();
                return RedirectToAction("CreateOrEdit", new { id = result.Model.GetType().GetProperty("Id").GetValue(result.Model) });
            }
            return BadRequest(ModelState);
        }
    }
}
