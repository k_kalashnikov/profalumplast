﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mst.Core.Models;
using Mst.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;

namespace Mst.Core.Controllers
{
    [Area("Public")]
    public class BasePublicModelController<TEntity, TPK> : BaseModelController<TEntity, TPK> where TEntity : class, IBaseModel<TEntity>
    {
        public BasePublicModelController(ApplicationDbContext _context) : base(_context)
        {
        }
    }
}
