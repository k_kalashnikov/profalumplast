﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mst.Core.Models;
using Mst.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;

namespace Mst.Core.Controllers
{
    public class BaseModelController<TEntity, TPK> : BaseController where TEntity : class, IBaseModel<TEntity>
    {
        public DbSet<TEntity> entity { get; set; }
        public BaseModelController(ApplicationDbContext _context) : base(_context)
        {
            entity = _context.Set<TEntity>();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual object Delete(TPK id)
        {
            var result = entity.DeleteById(id);
            context.SaveChanges();
            return new { result = result.Status, message = result.Message };
        }
    }
}
