﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mst.Core.Models;
using Microsoft.AspNetCore.StaticFiles;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Identity;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Mst.Core.Controllers
{
    public class BaseController : Controller
    {
        public ApplicationDbContext context { get; set; }
        public DirectoryConfig directories { get; set; }
        public BaseController(ApplicationDbContext _context)
        {
            context = _context;
            directories = context.Directories.FirstOrDefault() ?? new DirectoryConfig();
        }
        public virtual string GetMimeType(string filename)
        {
            string contentType;
            new FileExtensionContentTypeProvider().TryGetContentType(filename, out contentType);
            return contentType ?? "application/octet-stream";
        }
        public virtual IActionResult GetFile(string _fileName)
        {
            try
            {
                string path = Path.Combine(directories.BaseDirectory, _fileName);
                var stream = System.IO.File.OpenRead(path);
                return new FileStreamResult(stream, GetMimeType(path));
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
        public virtual List<string> GetModelStateErrors(ModelStateDictionary _modelState)
        {
            List<string> result = new List<string>();
            foreach (var itemVal in _modelState.Values)
            {
                foreach (var itemError in itemVal.Errors)
                {
                    result.Add(itemError.ErrorMessage);
                }
            }
            return result;
        }

        public virtual List<string> GetIndentityResultErrors(IdentityResult _identityResult)
        {
            return _identityResult.Errors?.Select(m => m.Description)?.ToList() ?? new List<string>();
        }
    }
}
