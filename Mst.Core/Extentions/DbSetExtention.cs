﻿using Mst.Core.Enums;
using Mst.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Microsoft.EntityFrameworkCore
{
    public delegate IQueryable<TEntity> IncludeDelegate<TEntity>(DbSet<TEntity> _entity) where TEntity : class;

    public static class DbSetExtention
    {
        public static bool IsExist<TEntity>(this DbSet<TEntity> _entity, TEntity _model)  where TEntity : class, IBaseModel<TEntity>
        {
            return (GetAll(_entity).AsNoTracking().FirstOrDefault(m => m.GetType().GetProperty("Id").GetValue(m).Equals(_model.GetType().GetProperty("Id").GetValue(_model))) is TEntity);
        }
        public static Result<TEntity> Append<TEntity>(this DbSet<TEntity> _entity, TEntity _model) where TEntity : class, IBaseModel<TEntity>
        {
            try
            {
                var result = new Result<TEntity>()
                {
                    Model = _entity.Add(_model).Entity,
                    Status = true,
                    Message = new LogMessage()
                    {
                        Message = $"Элемент {_model.GetType().Name} успешно добавлен",
                        Type = LogType.Success
                    }
                };
                return result;
            }
            catch (Exception e)
            {
                return new Result<TEntity>()
                {
                    Model = _model,
                    Status = false,
                    Message = new LogMessage()
                    {
                        Message = e.Message,
                        Type = LogType.Error
                    }
                };
            }
        }
        public static Result<TEntity> Refresh<TEntity>(this DbSet<TEntity> _entity, TEntity _model) where TEntity : class, IBaseModel<TEntity>
        {
            try
            {
                var result = new Result<TEntity>()
                {
                    Model = _entity.Update(_model).Entity,
                    Message = new LogMessage()
                    {
                        Type = LogType.Success,
                        Message = $"Элемент {_model.GetType().Name} успешно обновлен",
                    }
                };
                return result;

            }
            catch (Exception e)
            {
                return new Result<TEntity>()
                {
                    Model = _model,
                    Status = false,
                    Message = new LogMessage()
                    {
                        Message = e.Message,
                        Type = LogType.Error
                    }
                };
            }
        }
        public static Result<TEntity> Delete<TEntity>(this DbSet<TEntity> _entity, TEntity _model) where TEntity : class, IBaseModel<TEntity>
        {
            try
            {
                var result = new Result<TEntity>()
                {
                    Model = _entity.Remove(_model).Entity,
                    Message = new LogMessage()
                    {
                        Type = LogType.Success,
                        Message = $"Элемент {_model.GetType().Name} успешно удален",
                    }
                };
                return result;

            }
            catch (Exception e)
            {
                return new Result<TEntity>()
                {
                    Model = _model,
                    Status = false,
                    Message = new LogMessage()
                    {
                        Message = e.Message,
                        Type = LogType.Error
                    }
                };
            }
        }
        public static Result<TEntity> DeleteById<TEntity>(this DbSet<TEntity> _entity, object Id) where TEntity : class, IBaseModel<TEntity>
        {
            var model = GetById(_entity, Id, true);
            return Delete(_entity, model);
        }
        public static IQueryable<TEntity> GetAll<TEntity>(this DbSet<TEntity> _entity) where TEntity : class, IBaseModel<TEntity>
        {
            return Activator.CreateInstance<TEntity>().Include(_entity);
        }
        public static IQueryable<TEntity> Find<TEntity>(this DbSet<TEntity> _entity, Expression<Func<TEntity, bool>> _predicate, bool tracking = false) where TEntity : class, IBaseModel<TEntity>
        {
            return (tracking) ? GetAll(_entity).Where(_predicate) : GetAll(_entity).AsNoTracking().Where(_predicate);
        }
        public static TEntity GetById<TEntity>(this DbSet<TEntity> _entity, object id, bool tracking = false) where TEntity : class, IBaseModel<TEntity>
        {
            return (tracking) ? GetAll(_entity).FirstOrDefault(m => m.GetType().GetProperty("Id").GetValue(m).Equals(id)) : GetAll(_entity).AsNoTracking().FirstOrDefault(m => m.GetType().GetProperty("Id").GetValue(m).Equals(id));
        }
        public static bool IsExist<TEntity>(this DbSet<TEntity> _entity, object id) where TEntity : class, IBaseModel<TEntity>
        {
            return (GetAll(_entity).AsNoTracking().FirstOrDefault(m => m.GetType().GetProperty("Id").GetValue(m).Equals(id)) is TEntity);
        }
    }
}
