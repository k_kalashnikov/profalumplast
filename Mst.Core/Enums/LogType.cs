﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mst.Core.Enums
{
    public enum LogType
    {
        Success = 0,
        Warning = 1,
        Error = 2
    }
}
