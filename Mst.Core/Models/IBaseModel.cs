﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Mst.Core.Models
{
    public interface IBaseModel<TEntity> where TEntity : class
    {
        IQueryable<TEntity> Include(DbSet<TEntity> _entity);
    }
}
