﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Mst.Core.Models
{
    public class UserAccess : IBaseModel<UserAccess>
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int  AccessId { get; set; }
        public ApplicationUser User { get; set; }
        public ApplicationAccess Access { get; set; }

        public IQueryable<UserAccess> Include(DbSet<UserAccess> _entity)
        {
            return _entity.Include(m => m.Access).Include(m => m.User);
        }
    }
}
