﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Mst.Core.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser, IBaseModel<ApplicationUser>
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public override string Email { get; set; }
        [DataType(DataType.PhoneNumber)]
        public override string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        [DataType(DataType.ImageUrl)]
        public string Avatar { get; set; }

        public virtual ICollection<UserAccess> Accesses { get; set; }
        public IQueryable<ApplicationUser> Include(DbSet<ApplicationUser> _entity)
        {
            return _entity.Include(m => m.Accesses).ThenInclude(m => m.Access);
        }
    }
}
