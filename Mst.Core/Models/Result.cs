﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mst.Core.Models
{
    public class Result<TEntity> where TEntity : class, IBaseModel<TEntity>
    {
        public TEntity Model { get; set; }
        public LogMessage Message { get; set; }
        public bool Status { get; set; }
    }
}
