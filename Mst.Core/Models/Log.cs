﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Mst.Core.Models
{
    public class Log : IBaseModel<Log>
    {
        public int Id { get; set; }
        public ICollection<LogMessage> Messages { get; set; }

        public IQueryable<Log> Include(DbSet<Log> _entity)
        {
            return _entity.Include(m => m.Messages);
        }
    }
}
