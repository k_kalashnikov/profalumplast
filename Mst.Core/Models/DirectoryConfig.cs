﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Mst.Core.Models
{
    public class DirectoryConfig : IBaseModel<DirectoryConfig>
    {
        public int Id { get; set; }
        public string BaseDirectory { get; set; }

        public IQueryable<DirectoryConfig> Include(DbSet<DirectoryConfig> _entity)
        {
            return _entity;
        }
    }
}
