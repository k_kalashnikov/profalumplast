﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Mst.Core.Models
{
    public class RoleAccess : IBaseModel<RoleAccess>
    {
        public int Id { get; set; }
        public int AccessId { get; set; }
        public string RoleId { get; set; }

        public ApplicationRole Role { get; set; }
        public ApplicationAccess Access { get; set; }

        public IQueryable<RoleAccess> Include(DbSet<RoleAccess> _entity)
        {
            return _entity.Include(m => m.Access).Include(m => m.Role);
        }
    }
}
