﻿using Mst.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Mst.Core.Models
{
    public class LogMessage : IBaseModel<LogMessage>
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public LogType Type { get; set; }
        public Log Log { get; set; }
        public int LogId { get; set; }

        public IQueryable<LogMessage> Include(DbSet<LogMessage> _entity)
        {
            return _entity.Include(m => m.Log);
        }
    }
}
