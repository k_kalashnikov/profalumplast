﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Mst.Core.Models
{
    public class ApplicationRole : IdentityRole, IBaseModel<ApplicationRole>
    {
        public virtual ICollection<RoleAccess> Accesses { get; set; }

        public IQueryable<ApplicationRole> Include(DbSet<ApplicationRole> _entity)
        {
            return _entity.Include(m => m.Accesses).ThenInclude(m => m.Access);
        }
    }
}
