﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Mst.Core.Models;
using System.Reflection;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Mst.Core.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public DbSet<Log> Logs { get; set; }
        public DbSet<LogMessage> LogMessage { get; set; }
        public DbSet<DirectoryConfig> Directories { get; set; }
        public DbSet<ApplicationAccess> Accesses { get; set; }
        public DbSet<RoleAccess> RoleAccess { get; set; }
        public DbSet<UserAccess> UserAccess { get; set; }
        public DbSet<News> News { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbUpdateConcurrencyException e)
            {
                foreach (var entry in e.Entries)
                {
                    if (entry.Entity.GetType().GetTypeInfo().IsSubclassOf(typeof(IdentityUser)))
                    {
                        var databaseEntity = Users.AsNoTracking().Single(p => p.Id.Equals(entry.Property("Id").CurrentValue));
                        var databaseEntry = Entry(databaseEntity);
                        foreach (var property in entry.Metadata.GetProperties())
                        {
                            var proposedValue = entry.Property(property.Name).CurrentValue;
                            var originalValue = entry.Property(property.Name).OriginalValue;
                            var databaseValue = databaseEntry.Property(property.Name).CurrentValue;
                            entry.Property(property.Name).OriginalValue = databaseEntry.Property(property.Name).CurrentValue;
                        }
                    }
                    else
                    {
                        throw new NotSupportedException("Не знаю, как обрабатывать конфликты параллелизма для " + entry.Metadata.Name);
                    }
                }
                return base.SaveChanges();
            }
            catch (Exception e)
            {
                throw new NotSupportedException($"Не знаю как обрабатывать данное исключение {e.Message}");
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<RoleAccess>().HasOne(m => m.Access).WithMany(m => m.Roles).HasForeignKey(m => m.AccessId);
            builder.Entity<RoleAccess>().HasOne(m => m.Role).WithMany(m => m.Accesses).HasForeignKey(m => m.RoleId);

            builder.Entity<UserAccess>().HasOne(m => m.Access).WithMany(m => m.Users).HasForeignKey(m => m.AccessId);
            builder.Entity<UserAccess>().HasOne(m => m.User).WithMany(m => m.Accesses).HasForeignKey(m => m.UserId);

        }

        public void Seed(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            var needRoles = new List<string>() { "Admin", "Write", "ReadOnly", "User", "Guest" };
            foreach (var item in needRoles)
            {
                if (!Roles.Any(n => n.Name.Equals(item)))
                {
                    var newRole = new ApplicationRole()
                    {
                        Name = item
                    };
                    var result = roleManager.CreateAsync(newRole).Result;
                    if (!result.Succeeded)
                    {
                        var mess = "";
                        result.Errors.Select(m => m.Description).ToList().ForEach(m => mess += "\n" + m);
                        throw new Exception(mess);
                    }
                }
            }
        }
    }
}
