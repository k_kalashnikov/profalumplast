﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;


namespace Mst.Core.Models
{
    public class News : IBaseModel<News>
    {
        [Display(Name = "Id")]
        public int Id { get; set; }
        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        [Display(Name = "Полное описание")]
        public string Descrioption { get; set; }

        public IQueryable<News> Include(DbSet<News> _entity)
        {
            return _entity;
        }
    }
}
