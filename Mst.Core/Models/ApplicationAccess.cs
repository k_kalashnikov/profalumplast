﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Mst.Core.Models
{
    public class ApplicationAccess : IBaseModel<ApplicationAccess>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<RoleAccess> Roles { get; set; }
        public virtual ICollection<UserAccess> Users { get; set; }
        public IQueryable<ApplicationAccess> Include(DbSet<ApplicationAccess> _entity)
        {
            return _entity.Include(m => m.Roles).ThenInclude(m => m.Role).Include(m => m.Users).ThenInclude(m => m.User);
        }
    }
}
