﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Mst.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mst.Core
{
    public class Program
    {
        public class MigrationsContextFactory : IDbContextFactory<ApplicationDbContext>
        {
            public ApplicationDbContext Create(DbContextFactoryOptions options)
            {
                var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
                builder.UseSqlServer("Data Source=localhost;Initial Catalog=mst.server;Integrated Security=True;", b => b.MigrationsAssembly("Mst.Web"));
                return new ApplicationDbContext(builder.Options);
            }
            public static void Main(string[] args)
            {
            }
        }
    }
}
