﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mst.Core.ViewModels
{
    public class BaseModelViewModel<TModel> where TModel : class
    {
        public BaseModelViewModel()
        {
            model = Activator.CreateInstance<TModel>();
        }
        public BaseModelViewModel(TModel _model)
        {
            model = _model;
        }
        public TModel model { get; set; }
    }
}
