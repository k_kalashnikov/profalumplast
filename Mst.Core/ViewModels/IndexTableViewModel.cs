﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

namespace Mst.Core.ViewModels
{
    public class IndexTableViewModel
    {
        public IndexTableViewModel()
        {
            Items = new List<List<string>>();
            ColumnNames = new List<string>();
        }
        public IndexTableViewModel(List<object> _models) : this()
        {
            ColumnNames = _models.First().GetType().GetProperties().Where(m => !(m.GetCustomAttributes().Any()))?.Select(m => m.Name).ToList() ?? new List<string>();
            foreach (var itemModel in _models)
            {
                foreach (var itemProp in itemModel.GetType().GetProperties())
                {
                    var temp = new List<string>();
                    temp.Add(itemProp.GetValue(itemModel).ToString());
                }
            }
        }

        public List<List<string>> Items { get; set; }
        public List<string> ColumnNames { get; set; }
    }
}
