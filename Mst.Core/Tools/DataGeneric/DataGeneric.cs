﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Mst.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Mst.Core.Tools.DataGeneric
{
    public class DataGeneric<TContext> where TContext : DbContext
    {
        public TContext context { get; set; }
        public DataGeneric(TContext _context)
        {
            context = _context;
        }

        public List<DataItem> GetDataList()
        {
            var factory = new ClrPropertySetterFactory();
            List<DataItem> result = context.GetType()
                .GetRuntimeProperties()
                .Where(p => !p.IsStatic()
                         && !p.GetIndexParameters().Any()
                         && p.DeclaringType != typeof(DbContext)
                         && p.PropertyType.GetTypeInfo().IsGenericType
                         && p.PropertyType.GetGenericTypeDefinition() == typeof(DbSet<>))
                .OrderBy(p => p.Name)
                .Select(p => new DataItem()
                            {
                                Name = p.Name,
                                Type = p.PropertyType.GetTypeInfo().GenericTypeArguments.Single()
                            })
                .ToList();
            return result;
        }
    }
}
