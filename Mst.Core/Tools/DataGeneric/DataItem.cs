﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mst.Core.Tools.DataGeneric
{
    public class DataItem
    {
        public Type Type { get; set; }
        public string Name { get; set; }
    }
}
